[Sample json format](/Generate-Json-from-Excel-Files_sample.json)

#### Prerequisites


- Add your path in the code that you keep Excel workbooks where json file wil be generated.
- Add "Microsoft Scripting Runtime" to references.


Code will iterate workbooks in specified path and translate cell values into as sample json provided.