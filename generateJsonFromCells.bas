Attribute VB_Name = "generateJsonFromCells"
Option Explicit
'Please update address variable below with your path(without '\' at the end) to iterate all workbooks and save result .json file
'Sample Path: "C:\Users\fam\Desktop\excel-workbooks"
Private Const address = "***"

Dim counterMain, counterSub, i, rightLenght, k, counterTotal As Integer
Dim tempTextSub, tempTextMain, tempCustomerName, tempFullPath, tempWkName, tempShName, tempJson, tempCellContent, content As String
Dim Fileout As Object
Dim fsoTemp As FileSystemObject

Sub toJson()

Application.ScreenUpdating = False
Application.DisplayAlerts = False

If address = "***" Then Exit Sub

content = Empty
tempTextMain = Empty
tempTextSub = Empty
counterSub = 0
counterMain = 1
tempJson = Empty
tempFullPath = Empty
tempWkName = Empty
tempShName = Empty
tempCellContent = Empty
counterTotal = 1

loopFolders address
tempJson = "[" & tempJson & vbCrLf & "]"
writeJson tempJson
'Debug.Print tempJson

Application.ScreenUpdating = True
Application.DisplayAlerts = True

End Sub

Private Function writeJson(ByVal content As String)

Set fsoTemp = CreateObject("Scripting.FileSystemObject")
Set Fileout = fsoTemp.CreateTextFile(address & "\cells.json", True, True)

Fileout.Write content
Fileout.Close

Set fsoTemp = Nothing
Set Fileout = Nothing

End Function

Private Function getExcelWork(ByVal tempTextMainTemp As String, ByVal tempTextSubTemp As String, ByVal customerName As String, _
ByVal fileName As String, ByVal sheetName As String)

'Debug.Print sene & "-" & tempTextMainTemp & "-" & tempTextSubTemp & vbCrLf & customerName & vbCrLf & fileName & vbCrLf & sheetName & vbCrLf & _
path & vbCrLf & "-----------------" & vbCrLf

tempJson = tempJson & vbCrLf _
& "{" & vbCrLf _
& Chr(34) & "totalCounter" & Chr(34) & ":" _
& Chr(34) & counterTotal & Chr(34) & "," & vbCrLf _
& Chr(34) & "id" & Chr(34) & ":" _
& Chr(34) _
& tempTextMainTemp _
& "*" _
& tempTextSubTemp _
& Chr(34) & "," & vbCrLf & Chr(34) & "customerName" & Chr(34) & ":" & Chr(34) _
& customerName _
& Chr(34) & "," & vbCrLf & Chr(34) & "fileName" & Chr(34) & ":" & Chr(34) _
& fileName _
& Chr(34) & "," & vbCrLf & Chr(34) & "sheetName" & Chr(34) & ":" & Chr(34) _
& sheetName _
& Chr(34) & "," & vbCrLf & Chr(34) & "cells" & Chr(34) & ":{"

counterTotal = counterTotal + 1

End Function

Private Function getCells(ByVal cellAddress As String, ByVal cellContent As String)

'writeJson vbCrLf _
'& Chr(34) & cellAddress & Chr(34) _
'& ":" & Chr(34) & cellContent & Chr(34) _
'& ","

tempJson = tempJson & vbCrLf & Chr(34) & cellAddress & Chr(34) _
& ":" & Chr(34) & cellContent & Chr(34) & ","
'& Chr(34) & "cellAdress2" & Chr(34) _
'& ":" & Chr(34) & "cellContent2" & Chr(34) _
'& "}},}"

End Function

Private Function getCellTemp()

tempJson = tempJson & vbCrLf _
& Chr(34) & "cellAdress1" & Chr(34) _
& ":" & Chr(34) & "cellContent1" & Chr(34) _
& "," & vbCrLf _
& Chr(34) & "cellAdress2" & Chr(34) _
& ":" & Chr(34) & "cellContent2" & Chr(34) _
& "}},"

End Function

Private Function getCustomerName(ByVal completeString As String) As String

If LCase(completeString) = LCase(address) Then
getCustomerName = "No customer"
Exit Function
End If

getCustomerName = Replace(Replace(LCase(completeString), LCase(address), ""), "\", "")

End Function

Private Function fixString(ByVal fullPath As String)
 
For i = 1 To Len(fullPath)
If Mid(fullPath, i, 1) = "\" Or Mid(fullPath, i, 1) = Chr(34) Then
Mid(fullPath, i, 1) = "+"
End If
Next i
fixString = fullPath
fullPath = Empty

End Function

Private Function doStuff(ByVal excelFileString As Variant)
    
    Dim sh As Worksheet
    Dim wk As Workbook
    Dim tempCell As Variant
    
    On Error GoTo HataDevam
    Set wk = Application.Workbooks.Open(excelFileString, UpdateLinks:=False, ReadOnly:=True)
        counterMain = counterMain + 1
        tempCustomerName = getCustomerName(wk.path)
            'tempFullPath = getPath(wk.path)
        tempWkName = fixString(wk.Name)
    For Each sh In wk.Worksheets
        tempShName = fixString(sh.Name)
                counterSub = counterSub + 1
                If counterSub < 10 Then
                tempTextSub = "0" & counterSub
                Else
                tempTextSub = counterSub
                End If
            getExcelWork tempTextMain, tempTextSub, tempCustomerName, tempWkName, tempShName
            tempShName = Empty
    If WorksheetFunction.CountA(sh.Cells) <> 0 Then
        For Each tempCell In SelectActualUsedRange(sh).Cells
            If tempCell <> Empty Then
            On Error Resume Next
            getCells tempCell.address, CleanTrim(tempCell.Value, False)
            'Debug.Print CleanTrim(tempCell.Value)
            End If
        Next
        tempJson = Left(tempJson, Len(tempJson) - 1)
        tempJson = tempJson & "}},"
        Else
        getCellTemp
    End If

    Next sh
             
tempFullPath = Empty
tempWkName = Empty
tempCustomerName = Empty
counterSub = 0
tempTextSub = Empty
wk.Close False
Set wk = Nothing
    
Exit Function
    
HataDevam:
Debug.Print "hata=" & excelFileString
counterSub = 0
tempTextSub = Empty
tempFullPath = Empty
tempWkName = Empty
tempCustomerName = Empty
On Error Resume Next
wk.Close False
Set wk = Nothing

End Function

Function CleanTrim(ByVal S As String, Optional ConvertNonBreakingSpace As Boolean = True) As String
  Dim X As Long, CodesToClean As Variant
  CodesToClean = Array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, _
                       21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 34, 92, 127, 129, 141, 143, 144, 157)
  If ConvertNonBreakingSpace Then S = Replace(S, Chr(160), " ")
  For X = LBound(CodesToClean) To UBound(CodesToClean)
    If InStr(S, Chr(CodesToClean(X))) Then S = Replace(S, Chr(CodesToClean(X)), "")
  Next
  CleanTrim = WorksheetFunction.Trim(S)
End Function

Private Function loopFolders(ByVal patika As String)

Dim fso As Object
Dim fsoFolder As Folder

Set fso = CreateObject("Scripting.FileSystemObject")
                
        If fso.FolderExists(patika) = False Then
        MsgBox "Folder does not exist!"
        Exit Function
        End If
        
Set fsoFolder = fso.GetFolder(patika)

Dim fil As File
For Each fil In fsoFolder.Files
    
    If InStr(LCase(fil), ".xls") > 0 Then
    
        If counterMain < 10 Then
        tempTextMain = "00" & counterMain
        ElseIf counterMain > 9 And counterMain < 100 Then
        tempTextMain = "0" & counterMain
        Else
        tempTextMain = counterMain
        End If
        
    doStuff fil
    
    End If
Next fil

Dim fol As Folder
For Each fol In fsoFolder.SubFolders
    loopFolders fol.path
Next fol

Set fso = Nothing
Set fsoFolder = Nothing

End Function

Private Function SelectActualUsedRange(ByVal fileExcel As Worksheet) As Range
  Set SelectActualUsedRange = Nothing
  Dim FirstCell As Range, LastCell, yeniRange As Range
  Set LastCell = fileExcel.Cells(fileExcel.Cells.Find(What:="*", SearchOrder:=xlRows, _
      SearchDirection:=xlPrevious, LookIn:=xlValues).Row, _
      fileExcel.Cells.Find(What:="*", SearchOrder:=xlByColumns, _
      SearchDirection:=xlPrevious, LookIn:=xlValues).Column)
  Set FirstCell = fileExcel.Cells(fileExcel.Cells.Find(What:="*", After:=LastCell, SearchOrder:=xlRows, _
      SearchDirection:=xlNext, LookIn:=xlValues).Row, _
      fileExcel.Cells.Find(What:="*", After:=LastCell, SearchOrder:=xlByColumns, _
      SearchDirection:=xlNext, LookIn:=xlValues).Column)
  Debug.Print FirstCell.address
  Set SelectActualUsedRange = Range(Replace(FirstCell.address, "$", ""), Replace(LastCell.address, "$", ""))
  Set FirstCell = Nothing
  Set LastCell = Nothing
  Set fileExcel = Nothing
  
End Function
